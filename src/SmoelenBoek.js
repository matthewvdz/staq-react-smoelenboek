import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css'

function SmoelenBoek() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h1 className="App-title">SmoelenBoek</h1>
      </header>
      <p className="App-intro">
        Wie o wie staat er in het grote boek?
      </p>
    </div>
  );
}

export default SmoelenBoek;
